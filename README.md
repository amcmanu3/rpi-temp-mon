# Rpi Temp Mon



## Getting started

First make sure you have python installed. On RPI you can run `sudo apt-get install python3`
Then create a virtual environment running `python3 -m venv .venv` inside Rpi-Temp-Mon.
To install the project dependencies run: 
```
source .venv/bin/activate
pip3 install -r requirements.txt
```
Once that is complete you can start the program by running `python3 temp_warning.py`