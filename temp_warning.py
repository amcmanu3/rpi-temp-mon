from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from tzlocal import get_localzone
from datetime import datetime
import os
import sys
import json
import time
import uuid
import requests
import socket
import threading

from app.classes.tempSensor import TempSensor

CONFIG_TEMPLATE = {
    "sensor-name": "",
    "ntfy-creds": "",
    "ntfy-channel": "",
    "ntfy-server-address": "",
    "limit-upper-celsius": 34.5,
    "limit-lower-celsius": 10.0,
    "emergency-title": "",
    "vitals-check-in": True,
    "vitals-check-in-time": "00:00",
    "monitor-upper": True,
    "monitor-lower": False,
}


class TempMon:
    def __init__(self):
        self.TZ = get_localzone()
        self.current_temp = 0.0
        self.last_checked = "Init"
        self.email_authentication = True
        self.sensor = False
        self.startup = True
        self.temp_high = False
        self.temp_low = False
        self.high_count = 0
        self.low_count = 0
        self.network = False
        self.polling = False
        self.polling_thread = threading.Thread(
            target=self.temp_watcher, name="Temp_polling"
        )
        self.temp_sensor = TempSensor()
        self.temp_sensor.find_device()
        self.polling_thread.start()
        self.check_internet()
        try:
            with open("config.json", "r", encoding="utf-8") as config:
                self.current_config = json.load(config)
        except:
            self.current_config = {}
        self.get_config_diff()
        """
        Main method. Starter
        """
        # Checks current temperature of sensor in celsius
        enviro_data = self.temp_sensor.get_temp()
        temp = 0
        if enviro_data:
            temp, _hum = enviro_data
            now = datetime.now()
            self.last_checked = now.strftime("%m/%d %H:%M:%S")
            self.sensor = True
        temp_f = round((temp * 9 / 5) + 32, 1)
        self.current_temp = temp_f
        self.send_ntfy(
            "Temp Watcher Init",
            f"Current Temp {temp_f}.\n Warning Set to {round((self.current_config['limit-upper-celsius'] * 9 / 5) + 32, 1)}\nCurrent IP set to: {self.get_local_ip()}\nSensor registered: {self.sensor}",
            "",
            ["heavy_check_mark"],
            3,
        )
        if self.current_config["vitals-check-in"]:
            self.start_schedule()
        self.startup = False

    def check_internet(self):
        try:
            requests.get("https://ntp.org", timeout=1)
            self.network = True
        except Exception:
            try:
                requests.get("https://google.com", timeout=1)
                self.network = True
            except Exception:
                self.network = False
        return self.network

    def get_local_ip(_self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            return s.getsockname()[0]
        except Exception:
            ip = "127.0.0.1"
        finally:
            s.close()
        return ip

    def temp_watcher(self):
        """
        Main loop for temp checks
        """
        self.polling = True
        while self.polling:
            # Checks current temperature of sensor in celsius
            enviro_data = self.temp_sensor.get_temp()
            temp = 0
            if enviro_data:
                temp, _hum = enviro_data
                now = datetime.now()
                self.last_checked = now.strftime("%m/%d %H:%M:%S")
                if not self.sensor:
                    self.sensor = True
                    self.send_ntfy(
                        "Sensor reconnected!",
                        f"Current Temp {round((temp * 9 / 5) + 32, 1)}.\n Warning Set to {round((self.current_config['limit-upper-celsius'] * 9 / 5) + 32, 1)}\nCurrent IP set to: {self.get_local_ip()}",
                        "",
                        ["heavy_check_mark"],
                        3,
                    )
            else:
                if self.sensor:
                    self.sensor = False
                    self.send_ntfy(
                        "Sensor Absent",
                        f"Unable to make contact with SHT41 or 45. Please check USB connection\nCurrent IP set to: {self.get_local_ip()}",
                        "",
                        ["heavy_check_mark"],
                        3,
                    )
                continue
            temp_f = round((temp * 9 / 5) + 32, 1)
            if not self.network and self.check_internet():
                self.send_ntfy(
                    "Network Connection Restored",
                    f"Current Temp {temp_f}.\n Warning Set to {round((self.current_config['limit-upper-celsius'] * 9 / 5) + 32, 1)}\nCurrent IP set to: {self.get_local_ip()}",
                    "",
                    ["heavy_check_mark"],
                    3,
                )
            self.current_temp = temp_f
            if (
                int(temp) >= int(self.current_config["limit-upper-celsius"])
                and self.current_config["monitor-upper"]
            ):
                if not self.temp_high:
                    self.high_count += 1
                    if self.high_count >= 9:
                        self.high_count = 0
                        self.send_ntfy(
                            f"{self.current_config['emergency-title']}: TEMP HIGH!!",
                            f"Last reported temp was high\nTemp: {temp_f} F",
                            "",
                            ["rotating_light"],
                            5,
                        )
                        self.temp_high = True
            elif (
                int(temp) <= int(self.current_config["limit-lower-celsius"])
                and self.current_config["monitor-lower"]
            ):
                if not self.temp_low:
                    self.low_count += 1
                    if self.low_count >= 9:
                        self.low_count = 0
                        self.send_ntfy(
                            f"{self.current_config['emergency-title']}: TEMP LOW!!",
                            f"Last reported temp was low\nTemp: {temp_f} F",
                            "",
                            ["rotating_light"],
                            5,
                        )
                        self.temp_low = True
            else:
                if self.temp_high or self.temp_low:
                    self.temp_high = False
                    self.temp_low = False
                    self.send_ntfy(
                        "ALARM CLEARED",
                        f"Temperature has settled into acceptable limits.\nCurrent temp: {temp_f} F",
                        "",
                        ["nerd_face"],
                        3,
                    )
                print(f"temp is good. Current Temp: {temp_f} F")
            time.sleep(1)

    def get_config(self):
        return self.current_config

    def save_config(self, data, time_changed=False):
        json_object = json.dumps(data, indent=4)
        if (data["vitals-check-in"] and time_changed) and not self.startup:
            self.update_midnight(data["vitals-check-in-time"])
        if not self.startup:
            self.send_ntfy(
                "Config Updated!",
                f"Upper warning set to: {round((self.current_config['limit-upper-celsius'] * 9 / 5) + 32, 1)}\nEnabled: {self.current_config['monitor-upper']}\nLower warning set to: {round((self.current_config['limit-lower-celsius'] * 9 / 5) + 32, 1)}\nEnabled: {self.current_config['monitor-lower']}",
                "",
                ["computer"],
                1,
            )
        with open("config.json", "w", encoding="utf-8") as config:
            config.write(json_object)
        os.chmod("config.json", 0o600)
        return

    def send_slack(self, n_type, message):
        if n_type == "init":
            slack_data = {
                "blocks": [
                    {
                        "type": "header",
                        "text": {
                            "type": "plain_text",
                            "text": "Temp Mon Init",
                            "emoji": True,
                        },
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"Temp monitor online at {datetime.now()}.",
                        },
                    },
                    {
                        "type": "header",
                        "text": {"type": "plain_text", "text": "Limits", "emoji": True},
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f'Upper limit monitoring set to {self.current_config["upper_limit"]}. Lower Limit monitoring set to {self.current_config["lower_limit"]}.',
                        },
                    },
                    {
                        "type": "header",
                        "text": {
                            "type": "plain_text",
                            "text": "Notifications",
                            "emoji": True,
                        },
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f'Slack notifications on. Sending SMS to: {self.current_config["perm-truck-engs"]}',
                        },
                    },
                ]
            }
        else:
            slack_data = {"text": message}
        headers = {"Content-type": "application/json"}
        requests.post(
            self.current_config["slack_url"], header=headers, data=slack_data, timeout=1
        )

    def send_test(self):
        self.send_ntfy(
            "Test Notification",
            "This has been a successful test of the temp mon notification system.",
            "",
            ["loudspeaker"],
            3,
        )

    def send_ntfy(self, title, message, attach_url, tags, priority):
        headers = {"Authorization": f"Bearer {self.current_config['ntfy-creds']}"}
        data = json.dumps(
            {
                "topic": self.current_config["ntfy-channel"],
                "message": message,
                "title": f"{title} - {self.current_config['sensor-name']}",
                "tags": tags,
                "priority": priority,
                "attach": "",
            }
        )
        try:
            response = requests.post(
                "https://notify.andrewmcmanus.net",
                headers=headers,
                data=data,
                timeout=1,
            )
        except requests.exceptions.ConnectionError as why:
            print(f"Failed to send NTFY with error {why}")

    def start_schedule(self):
        """
        Starts midnight vitals checks schedule
        """
        print("scheduling midnight vitals")
        time = self.current_config["vitals-check-in-time"].split(":")
        self.midnight_schedule = BackgroundScheduler(timezone=self.TZ)
        self.midnight_schedule.start()
        # add checks for overnight schedule
        self.midnight_schedule.add_job(
            self.get_midnight_vitals,
            CronTrigger.from_crontab(f"{time[1]} {time[0]} * * *", timezone=self.TZ),
            id="midnight",
        )

    def get_midnight_vitals(self):
        self.send_ntfy(
            "Vitals Check-in",
            f"Service Okay.\n\n Current temp {self.current_temp} F",
            "",
            ["heavy_check_mark"],
            3,
        )

    def get_config_diff(self):
        master_config = CONFIG_TEMPLATE
        try:
            user_config = self.current_config
        except:
            user_config = master_config
            keys = list(user_config.keys())
            keys.sort()
            sorted_data = {i: user_config[i] for i in keys}
            self.save_config(user_config)
            return
        items_to_del = []

        # Iterate through user's config.json and check for
        # Keys/values that need to be removed
        for key in user_config:
            if key not in master_config.keys():
                items_to_del.append(key)

        # Remove key/values from user config that were staged
        for item in items_to_del[:]:
            del user_config[item]

        # Add new keys to user config.
        for key, value in master_config.items():
            if key not in user_config.keys():
                user_config[key] = value
        # Call helper to set updated config.
        keys = list(user_config.keys())
        keys.sort()
        sorted_data = {i: user_config[i] for i in keys}
        self.save_config(sorted_data)

    def update_midnight(self, time):
        time = time.split(":")
        self.send_ntfy(
            "Schedule Update",
            f"Rescheduling Vitals to {self.current_config['vitals-check-in-time']}",
            "",
            ["computer"],
            3,
        )
        self.midnight_schedule.remove_job("midnight")
        self.midnight_schedule.add_job(
            self.get_midnight_vitals,
            CronTrigger.from_crontab(f"{time[1]} {time[0]} * * *", timezone=self.TZ),
            id="midnight",
        )
