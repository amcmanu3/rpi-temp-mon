from flask import Flask, request, render_template, redirect, jsonify
from temp_warning import TempMon
import threading
import sys
import signal
import time

app = Flask(
    __name__, template_folder="frontend/templates", static_folder="frontend/static"
)
temp_mon = TempMon()


@app.route("/", methods=["POST", "GET"])
def index():
    if request.method == "GET":
        data = temp_mon.get_config()
        stats = {"temp": temp_mon.current_temp, "lastChecked": temp_mon.last_checked}
        return render_template("index.html", title="Welcome", data=data, stats=stats)

    elif request.method == "POST":
        data = temp_mon.get_config()
        time_changed = False
        if data["vitals-check-in-time"] != request.form.get("vitals-check-in-time"):
            time_changed = True
        data["ntfy-channel"] = str(request.form.get("ntfy-channel"))
        data["ntfy-creds"] = str(request.form.get("ntfy-creds"))
        data["ntfy-server-address"] = str(request.form.get("ntfy-server-address"))
        data["sensor-name"] = str(request.form.get("sensor-name"))
        data["limit-upper-celsius"] = float(request.form.get("limit-upper-celsius"))
        data["limit-lower-celsius"] = float(request.form.get("limit-lower-celsius"))
        data["emergency-title"] = request.form.get("emergency-title")
        data["vitals-check-in"] = bool(request.form.get("vitals-check-in", False))
        data["monitor-upper"] = bool(request.form.get("monitor-upper", False))
        data["monitor-lower"] = bool(request.form.get("monitor-lower", False))
        data["vitals-check-in-time"] = request.form.get("vitals-check-in-time")
        temp_mon.save_config(data, time_changed)
        return redirect("/")


@app.route("/notify/test/", methods=["POST"])
def send_test():
    temp_mon.send_test()
    response = {"status": "ok"}
    return jsonify(response)


@app.route("/api/temp", methods=["GET"])
def get_temp():
    temp = {
        "status": "ok",
        "data": {"temp": temp_mon.current_temp, "lastChecked": temp_mon.last_checked},
    }
    return jsonify(temp)


@app.route("/api/rescan/", methods=["POST"])
def rescan_devices():
    temp_mon.temp_sensor.find_device()
    return jsonify(
        {
            "status": "ok",
            "data": {
                "temp": temp_mon.current_temp,
                "lastChecked": temp_mon.last_checked,
            },
        }
    )


def main():
    if __name__ == "__main__":
        try:
            app.run("0.0.0.0", debug=True, use_reloader=False)
        except KeyboardInterrupt:
            print()
            print("Keyboard interrupt caught. Exiting...")
            sys.exit(0)


def sigint_handler(signal, frame):
    print("Sigterm caught. Exiting...")
    temp_mon.polling = False
    time.sleep(2)
    sys.exit(0)


signal.signal(signal.SIGINT, sigint_handler)

main()
