import logging
import serial
from serial import SerialException
from typing import Tuple
import serial.tools.list_ports

logger = logging.getLogger(__name__)


class TempSensor:
    def __init__(self):
        self.temp_avail = True
        self.temp = 0
        self.hum = 0
        self.polling = False
        self.fieldnames = ["sn", "temp", "hum", "touch"]
        self.port = None
        self.accepted_devices = {"vid": ["0x239A"], "pid": ["0x8153"]}
        self.serial = None

    def get_temp(self) -> Tuple[bool, tuple]:
        logger.info("Getting indoor temp using %s", self.port)
        if self.port:
            try:
                self.serial = serial.Serial(self.port)
                self.serial.reset_output_buffer()
                self.serial.readline()
                cur_line = self.serial.readline().strip().decode()
                self.serial.close()
            except SerialException as why:
                logger.error("Failed to get temp from serial device with error %s", why)
                return False
            data_list = cur_line.split(",")
            temp_data = {}
            for field in self.fieldnames:
                try:
                    temp_data[field] = data_list[self.fieldnames.index(field)]
                except IndexError as why:
                    temp_data[field] = None
                    logger.warning(
                        "Failed to recognize field name %s from device with error %s",
                        field,
                        why,
                    )
            if not self.temp_avail:
                return False
            # Offset sensor by 2 degrees
            try:
                return (
                    float(temp_data.get("temp")) - 3.4,
                    round(float(temp_data.get("hum"))),
                )
            except TypeError as why:
                logger.error(
                    "Failed to convert data from temp sensor to float with error %s",
                    why,
                )
        return False

    def find_device(self):
        ports = serial.tools.list_ports.comports()
        pids = []
        vids = []
        for pid in self.accepted_devices.get("pid"):
            pids.append(int(pid, 16))
        for vid in self.accepted_devices.get("vid"):
            vids.append(int(vid, 16))
        for port in ports:
            if port.vid in vids and port.pid in pids:
                logger.info("Found supported Adhfruit temp device on %s", port.device)
                self.port = port.device
                return
        self.port = None
